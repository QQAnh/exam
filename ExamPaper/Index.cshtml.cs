﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ExamPaper.Models;

namespace ExamPaper
{
    public class IndexModel : PageModel
    {
        private readonly ExamPaper.Models.ExamPaperContext _context;

        public IndexModel(ExamPaper.Models.ExamPaperContext context)
        {
            _context = context;
        }

        public IList<Product> Product { get;set; }

        public async Task OnGetAsync()
        {
            Product = await _context.Product
                .Include(p => p.ctx).ToListAsync();
        }
    }
}
