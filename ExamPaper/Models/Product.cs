﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamPaper.Models
{
    public class Product
    {
        private int productID;
        private string productName;
        private int supplierID;
        private int categoryID;
        private string quantityPerUnit;
        private string unitsPrice;
        private string unitsStock;
        private string unitsOnOrder;
        private string reorderLevel;
        private string discontinued;
        public  virtual Category ctx { get;  set; }

        public int ProductID { get => productID; set => productID = value; }
        public string ProductName { get => productName; set => productName = value; }
        public int SupplierID { get => supplierID; set => supplierID = value; }
        public int CategoryID { get => categoryID; set => categoryID = value; }
        public string QuantityPerUnit { get => quantityPerUnit; set => quantityPerUnit = value; }
        public string UnitsPrice { get => unitsPrice; set => unitsPrice = value; }
        public string UnitsStock { get => unitsStock; set => unitsStock = value; }
        public string UnitsOnOrder { get => unitsOnOrder; set => unitsOnOrder = value; }
        public string ReorderLevel { get => reorderLevel; set => reorderLevel = value; }
        public string Discontinued { get => discontinued; set => discontinued = value; }
    }
}
