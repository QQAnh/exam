﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamPaper.Models
{
    public class Category
    {
       
        private int categoryID;
        private string categoryName;
        private string description;
        private string picture;
       
        public Product Product { get; set; }
        public int CategoryID { get => categoryID; set => categoryID = value; }
        public string CategoryName { get => categoryName; set => categoryName = value; }
        public string Description { get => description; set => description = value; }
        public string Picture { get => picture; set => picture = value; }
    }
}
