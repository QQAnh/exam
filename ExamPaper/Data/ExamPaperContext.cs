﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ExamPaper.Models;

namespace ExamPaper.Models
{
    public class ExamPaperContext : DbContext
    {
        public ExamPaperContext (DbContextOptions<ExamPaperContext> options)
            : base(options)
        {
        }

        public DbSet<ExamPaper.Models.Product> Product { get; set; }

        public DbSet<ExamPaper.Models.Category> Category { get; set; }
    }
}
